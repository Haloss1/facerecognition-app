import React from 'react';
import './ImageLinkForm.css';

const ImageLinkForm = ({ onInputChange, onButtonSubmit }) => {
  return (
    <div>
      <p className='f2 white'>
        {'This Magic Brain Will Detect Faces in your Picture. Give it a try.'}
      </p>
      <div className='center f1'>
        <div className='center form pa4 br1 shadow-5'>
          <input
            className='f4 pa2 w-60 center'
            type='text'
            onChange={onInputChange}
            placeholder='Insert Image Url:'
          />
          <button
            onClick={onButtonSubmit}
            className='w-30 grow f4 link ph1 pv2 dib white bg-blue pointer br4 shadow-2 btnBG'
          >
            Detect
          </button>
        </div>
      </div>
    </div>
  );
};

export default ImageLinkForm;
