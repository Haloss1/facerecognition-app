import React from 'react';
import Tilt from 'react-tilt';
import './Logo.css';
import LogoImage from './Logo.png';
import ProfileImage from './Profile.png';

const Logo = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <a
        href='https://gitlab.com/Haloss1/facerecognition-app'
        className='ma4 mt0'
        style={{ textDecoration: 'none' }}
      >
        <Tilt
          className='tilt br2 shadow-2 pointer'
          options={{ max: 50 }}
          style={{ height: 160, width: 150 }}
        >
          <div className='pt2 pb0'>
            <img src={LogoImage} alt='logo' />
            <p className='white f5'>Face recognition</p>
          </div>
        </Tilt>
      </a>
      <div className='white f1 mt5'>Face recognition App</div>
      <a
        href='https://haloss1.netlify.app'
        className='ma4 mt0'
        style={{ textDecoration: 'none' }}
      >
        <Tilt
          className='tilt-profile br2 shadow-2 pointer'
          options={{ max: 50 }}
          style={{ height: '5em', width: '11em' }}
        >
          <div
            className='pt2 pb0'
            style={{ display: 'flex', justifyContent: 'space-around' }}
          >
            <img src={ProfileImage} alt='logo' style={{ width: '4em' }} />
            <p className='white f4'> Haloss1 </p>
          </div>
        </Tilt>
      </a>
    </div>
  );
};

export default Logo;
