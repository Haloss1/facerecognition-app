import React from 'react';
import './FaceRecognition.css';

const FaceRecognition = ({ imageUrl, box }) => {
  return (
    <div className='center pt3 ma'>
      <div className='absolute di mt2'>
        <img
          id='inputimage'
          src={imageUrl}
          alt=''
          width='500px'
          height='auto'
        />
        {box.map((boxItem) => (
          <div
            key={boxItem.id}
            className='bounding-box'
            style={{
              top: boxItem.topRow,
              bottom: boxItem.bottomRow,
              left: boxItem.leftCol,
              right: boxItem.rigthCol,
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default FaceRecognition;
